import datetime

from django.urls import reverse
from django.test import TestCase
from django.utils import timezone

from .models import Question


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)

    def test_was_published_recently_with_past_question(self):
        time = timezone.now() - datetime.timedelta(hours=24, minutes=0, seconds=0)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question2(self):
        time = timezone.now()
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)


def create_ques(ques_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=ques_text, pub_date=time)


class QuestionIndexViewTests(TestCase):
    def test_no_question(self):
        response = self.client.get(reverse('polls2:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        create_ques(ques_text="Past question", days=-30)
        response = self.client.get(reverse('polls2:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question>'])

    def test_future_question(self):
        create_ques(ques_text="Future question", days=30)
        response = self.client.get(reverse('polls2:index'))
        self.assertContains(response, "No polls are available")
        self.assertQuerysetEqual(response.context['latest_question_list'], [ ])

    def test_future_and_past_question(self):
        create_ques(ques_text="Past question", days=-30)
        create_ques(ques_text="Future question", days=30)
        response = self.client.get(reverse('polls2:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question>'])

    def test_two_past_question(self):
        create_ques(ques_text="Past question 1", days=-30)
        create_ques(ques_text="Past question 2", days=-5)
        response = self.client.get(reverse('polls2:index'))
        self.assertQuerysetEqual(response.context['latest_question_list'],
                                  ['<Question: Past question 2>', '<Question: Past question 1>'])

    # def test_past_question_without_choicees(self):
    #     create_ques(ques_text="Past question", days=-30)
    #     response = self.client.get(reverse('polls2:index'))
    #     self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question>'])


class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        future_question = create_ques(ques_text='Future question', days=5)
        url = reverse('polls2:detailofques', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_ques(ques_text='Past question', days=-5)
        url = reverse('polls2:detailofques', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)


class QuestionResultsViewTests(TestCase):
    def test_future_question(self):
        future_question = create_ques(ques_text='Future question', days=5)
        url = reverse('polls2:results', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        past_question = create_ques(ques_text='Past question', days=-5)
        url = reverse('polls2:results', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)
